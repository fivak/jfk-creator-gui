package test


import model.Frame
import util.{XmlParser, XmlReader}

import scala.swing.{Button, FileChooser, Label, MainFrame}

/**
 * Created by Filip on 2015-05-04.
 */

object xml extends App{

  //Some fun with xml as String

  val xmlText = XmlReader.getXMLAsString("frame.xml")
  val frame:Frame = XmlParser.getXMLAsFrame(xmlText)
  //println(frame)
  //println(xmlText)
  println(frame.lists.list.map(println))

}




