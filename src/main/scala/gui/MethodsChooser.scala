package gui

/**
 * Created by Filip on 2015-05-12.
 */

import scala.language.experimental.macros
class MethodsChooser {

  def cat(a:String) = {println("MIAAAAU")}
  def dog(a:String) = {println("HAUHAU")}
  def method(a:String) = {println("method")}

  def chooseMethod(methodName:String, methodCode:String, imports:String) : Any ={
    val a = new MethodsChooser
    val hi = "Hello"
    methodName match {
      case "cat" => a.getClass.getMethod(methodName,hi.getClass).invoke(a,"")
      case "dog" => a.getClass.getMethod(methodName,hi.getClass).invoke(a,"")
      case "method" => new MethodInvokeClass(methodCode,imports)
      case _ => println("Not defined method")
    }
  }
}
