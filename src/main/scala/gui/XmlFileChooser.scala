package gui

import java.io.File

import scala.swing.FileChooser

/**
 * This class help us to get correct xml file
 * Created by Filip on 2015-05-06.
 */
object XmlFileChooser {

  /**
   * Create window which allows to choose XmlFile to build Frame
   * @return Path to file or null
   */
  def getXmlFile(): String = {
    val chooser = new FileChooser(new File("."))
    val result = chooser.showOpenDialog(null)
    if (result == FileChooser.Result.Approve) {
      println(chooser.selectedFile)
      return chooser.selectedFile.toString
    }
    return null
  }
}
