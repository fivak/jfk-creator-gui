package gui

import java.awt.{Rectangle, Dimension, Color, Point}


import util.DataMock
import util.DataMock.City

import scala.collection.mutable.ArrayBuffer
import scala.swing.{ListView, Action, Button, Label,MenuBar,MenuItem,Menu}
/**
 * This class transforms from model class to scala.swing objects.
 * Created by fhalemba on 2015-05-08.
 */
object GuiObjectTransformer {

  /**
   * This method transforms objects from model.button.Button class to scala.swing.Button class.
   * @param modelButtonList Array of model.button.Button objects.
   * @return Array of scala.swing.Buttons objects.
   */
  def createButtons(modelButtonList:Array[model.button.Button]) : Array[Button] = {
    val guiButtonList: ArrayBuffer[Button] = ArrayBuffer[Button]()
    modelButtonList.map(modelButton => {
        guiButtonList += new Button {
          preferredSize = new Dimension(modelButton.weight,modelButton.height)
          minimumSize = preferredSize
          maximumSize = preferredSize
          borderPainted = true
          enabled = modelButton.enabled
          tooltip = modelButton.tooltip
          action = new Action(modelButton.text) {
            override def apply(): Unit = {
              val a = new MethodsChooser
              a.chooseMethod(modelButton.action,modelButton.methodCode,modelButton.imports)
            }
          }
        }
      }
    )
    guiButtonList.toArray
  }

  def createLabels(modelLabelList:Array[model.label.Label]) : Array[Label] = {
    val guiLabelList:ArrayBuffer[Label] = ArrayBuffer[Label]()
    modelLabelList.map( modelLabel =>{
        guiLabelList += new Label {
          text = modelLabel.text
        }
      }
    )
    guiLabelList.toArray
  }

  def createList(modelList:Array[model.list.List]) : Array[ListView[City]] = {
    val guiList:ArrayBuffer[ListView[City]] = ArrayBuffer[ListView[City]]()
    modelList.map( modelListObject =>{
        guiList += new ListView(DataMock.getDataToList())
      }
    )
    guiList.toArray
  }

  def createMenuBar(modelMenuBar:model.menubar.MenuBar) : MenuBar = {
    val guiMenuBar:MenuBar = new MenuBar()

    modelMenuBar.menus.menu.map(currentModelMenu =>{ //dla kazdego menu z menu bar
          var guiMenu:Menu = new Menu(currentModelMenu.text) //Stworz obiekt Menu
          currentModelMenu.menuItems.menuItem.map(currentModelMenuItem => //dla kazdego MenuItem z Menu
              guiMenu.contents += new MenuItem(currentModelMenuItem.text){ //Dodaje MenuItem do odpowiedniego Menu
              action = new Action(currentModelMenuItem.text) { //dla kazdego MenuItem dodaj akcje
                override def apply(): Unit = {
                  val a = new MethodsChooser
                  a.chooseMethod(currentModelMenuItem.action,currentModelMenuItem.methodCode,currentModelMenuItem.imports)
                }
              }
            }
          )
          guiMenuBar.contents+=guiMenu; //dodaj do menuBar kazde wypelnione menu
      }
    )
    guiMenuBar;
  }
}
