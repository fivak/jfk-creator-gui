package gui;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import static com.sun.tools.javac.Main.compile;


/**
 * Created by Sebastian on 2015-05-17.
 */
public class MethodInvokeClass {
    public MethodInvokeClass(String methodCode, String imports) throws Exception {
        String workingDir = System.getProperty("user.dir");
        String className = "InvokeClass";
        BufferedWriter bw1 = new BufferedWriter(new FileWriter(workingDir + File.separator + className + ".java"));
        bw1.newLine();
        bw1.write(imports);
        bw1.write("public class InvokeClass {");
        bw1.write(" public static void sampleMethod() {");
        bw1.write(methodCode);
        bw1.write("}");
        bw1.write("}");
        bw1.close();
        compile(new String[]{workingDir + File.separator + className + ".java"});
        ClassLoader classLoader1 = new URLClassLoader(new URL[] { new File(workingDir).toURI().toURL() });
        Class clazz1 = classLoader1.loadClass(className);
        Constructor ctr1 = clazz1.getConstructor(new Class[0]);
        Method method1 = clazz1.getMethod("sampleMethod", new Class[0]);
        Object obj1 = ctr1.newInstance(new Object[0]);
        method1.invoke(obj1, new Object[0]);
        File file = new File(workingDir + "\\InvokeClass.class");
        File file2 = new File(workingDir + "\\InvokeClass.java");
        file.delete();
        file2.delete();
    }
}
