package gui

import java.awt.{Dimension}

import _root_.model.Frame
import util.DataMock
import scala.swing._

/**
 * This class creates GUI which is based on xml file which is mapped on model.Frame class
 * Created by Filip on 2015-05-06.
 */
class GuiFrameTransformer(modelFrame:Frame) extends MainFrame {

  //Pobieranie podstawowych wlasciwosci okna
  title = modelFrame.title
  preferredSize = new Dimension(modelFrame.height, modelFrame.weight)
  centerOnScreen
  resizable = true

  //Transformacja z obiektow modelowych do obiektow scala.swing
  val guiButtonList:Array[Button] = GuiObjectTransformer.createButtons(modelFrame.buttons.button)
  val guiLabelList:Array[Label] = GuiObjectTransformer.createLabels(modelFrame.labels.label)
  val guiMenuBar = GuiObjectTransformer.createMenuBar(modelFrame.menuBar)
  val guiList = GuiObjectTransformer.createList(modelFrame.lists.list)

  contents = new BoxPanel(Orientation.Vertical) {
    guiButtonList.foreach(contents+=_)
    guiLabelList.foreach(contents+=_)
    menuBar = guiMenuBar;
    guiList.foreach(contents+=new ScrollPane(_))
    border = Swing.EmptyBorder(0, 0, 0, 0)
  }

}
