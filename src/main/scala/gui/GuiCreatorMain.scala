package gui

import _root_.model.Frame
import util.{XmlParser, XmlReader}


/**
 * Main class of project
 * Created by Filip on 2015-05-05.
 */
object GuiCreatorMain extends App {
  //val path = "frame.xml"
  val path = XmlFileChooser.getXmlFile()
  val xmlAsString = XmlReader.getXMLAsString(path)
  val frame:Frame = XmlParser.getXMLAsFrame(xmlAsString)
  val ui = new GuiFrameTransformer(frame)
  ui.visible = true
}



