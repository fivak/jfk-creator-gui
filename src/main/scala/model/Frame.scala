package model

import javax.xml.bind.annotation._

import model.button.Buttons
import model.label.Labels
import model.list.Lists
import model.menubar.MenuBar

/**
 * The main class of the model that contains all the other.
 * Created by Filip on 2015-05-04.
 */
@XmlRootElement(name = "frame")
@XmlAccessorType(XmlAccessType.FIELD)
case class Frame(title:String,
   @XmlAttribute(name = "height") height:Int,
   @XmlAttribute(name = "weight") weight:Int,
                 buttons:Buttons,
                 labels:Labels,
                 menuBar:MenuBar,
                 lists:Lists
                  ) {
  //Wymagane przez JAXB zeby byl domyslny konstruktor
  private def this() = this("",0,0,null,null,null,null)
}
