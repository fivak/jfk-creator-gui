package model.menubar

import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

import model.menu.Menus

/**
 * Created by Filip on 2015-05-11.
 */
@XmlRootElement(name = "menuBar")
@XmlAccessorType(XmlAccessType.FIELD)
case class MenuBar (
                     menus:Menus
                    ) {
  private def this() = this(null)
}
