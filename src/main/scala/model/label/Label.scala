package model.label

import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

/**
 * 		<label>
			<height>300</height>
			<weight>120</weight>
			<text>test2</text>
		  </label>
 * Created by Sebastian on 2015-05-10.
 */
@XmlRootElement(name = "label")
@XmlAccessorType(XmlAccessType.FIELD)
case class Label (
                    text:String
                  ) {
    //Wymagane przez JAXB zeby byl domyslny konstruktor
    private def this() = this(null)
}

