package model.label

import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}


/**
 * 	<labels>
		<label>
			<height>300</height>
			<weight>120</weight>
			<text>No button clicks registered</text>
		</label>
		<label>
			<height>300</height>
			<weight>120</weight>
			<text>test2</text>
		</label>
	  </labels>
 * Created by Sebastian on 2015-05-10.
 */
@XmlRootElement(name = "labels")
@XmlAccessorType(XmlAccessType.FIELD)
case class Labels(
                   label:Array[Label] = Array[Label]()
                   ) {
    private def this() = this(Array[Label]())
}
