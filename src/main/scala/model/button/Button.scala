package model.button

import java.awt.Color
import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

/**
 *	<button>
 *	   <text>Drugi button</text>
 *  </button>
 * Created by Filip on 2015-05-04.
 */
@XmlRootElement(name = "button")
@XmlAccessorType(XmlAccessType.FIELD)
case class Button(
                   text:String,
                   height:Int,
                   weight:Int,
                   tooltip:String,
                   enabled:Boolean,
                   action:String,
                   methodCode:String,
                   imports:String
                   ) {
  //Wymagane przez JAXB zeby byl domyslny konstruktor
  private def this() = this(null,40,40,"",true,"","","")
}
