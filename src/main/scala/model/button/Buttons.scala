package model.button

import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

/**
 * This class wraps the list of model.Button objects
 *	<buttons>
 *		 <button>
 *			  <text>Pierwszy button</text>
 *		 </button>
 *		 <button>
 *			  <text>Drugi button</text>
 *		 </button>
 *	</buttons>
 * Created by Filip on 2015-05-06.
 */
@XmlRootElement(name = "buttons")
@XmlAccessorType(XmlAccessType.FIELD)
case class Buttons(
                    button:Array[Button] = Array[Button]() //nazwa kolekcji musi miec nazwe jak pojedynczy element inaczej JAXB nie bangla
                   ) {
  private def this() = this(Array[Button]())
}
