package model.menu

import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

/**
 * Created by Filip on 2015-05-11.
 */
@XmlRootElement(name = "menus")
@XmlAccessorType(XmlAccessType.FIELD)
case class Menus(
                     menu:Array[Menu] = Array[Menu]()
                     ){
  private def this() = this(Array[Menu]())
}
