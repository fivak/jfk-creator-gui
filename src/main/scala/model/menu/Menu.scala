package model.menu

import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

import model.menuitem.MenuItems

/**
 * Created by Filip on 2015-05-11.
 */
@XmlRootElement(name = "menu")
@XmlAccessorType(XmlAccessType.FIELD)
case class Menu (
                     text:String,
                     menuItems:MenuItems
                     ) {
  private def this() = this(null,null)
}
