package model.list

import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

@XmlRootElement(name = "lists")
@XmlAccessorType(XmlAccessType.FIELD)
case class Lists(
                   list:Array[List] = Array[List]()
                   ) {
    private def this() = this(Array[List]())
}
