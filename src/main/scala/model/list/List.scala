package model.list

import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

@XmlRootElement(name = "list")
@XmlAccessorType(XmlAccessType.FIELD)
case class List (
                    text:String
                  ) {
    //Wymagane przez JAXB zeby byl domyslny konstruktor
    private def this() = this(null)
}

