package model.menuitem

import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

/**
 * Created by Filip on 2015-05-11.
 */
@XmlRootElement(name = "menuItem")
@XmlAccessorType(XmlAccessType.FIELD)
case class MenuItem (
                 text:String,
                 action:String,
                 methodCode:String,
                 imports:String
                 ) {
  private def this() = this("","","","")
}
