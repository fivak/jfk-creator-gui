package model.menuitem

import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

import model.menu.Menu

/**
 * Created by Filip on 2015-05-11.
 */
@XmlRootElement(name = "menuItems")
@XmlAccessorType(XmlAccessType.FIELD)
case class MenuItems(
                 menuItem:Array[MenuItem] = Array[MenuItem]()
                 ){
  private def this() = this(Array[MenuItem]())
}
