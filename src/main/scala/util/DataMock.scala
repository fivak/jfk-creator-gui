package util

/**
 * Created by fhalemba on 2015-05-16.
 */
object DataMock {

  case class City(name: String, country: String, population: Int, capital: Boolean)

  /**
   * Create mock data to swing List object
   * @return
   */
    def getDataToList() : List[City] ={

      val items = List(City("Lausanne", "Switzerland", 129273, false),
        City("Paris", "France", 2203817, true),
        City("New York", "USA", 8363710 , false),
        City("Berlin", "Germany", 3416300, true),
        City("Tokio", "Japan", 12787981, true))

      return items
    }


}
