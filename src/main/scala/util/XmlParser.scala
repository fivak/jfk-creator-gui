package util

import java.io.{File, StringReader}
import javax.xml.bind.JAXBContext

import model.Frame

/**
 * Created by Filip on 2015-05-05.
 */
object XmlParser {
  /**
   * Parse xml file saved as String to Frame object
   * @param xml XML file as String
   * @return Frame object
   */
  def getXMLAsFrame(xml:String):Frame = {
    val jaxbContext = JAXBContext.newInstance(classOf[Frame])
    val p = jaxbContext.createUnmarshaller().unmarshal(new StringReader(xml))
    p.asInstanceOf[Frame]
  }
  /**
   * Save to xml file frame model object.
   * @param frame Reference to frame model object.
   */
  def makeXML(frame:Frame) = {
    val jaxbContext = JAXBContext.newInstance(classOf[Frame])
    jaxbContext.createMarshaller().marshal(frame,new File("aaa.xml"))
  }
}
