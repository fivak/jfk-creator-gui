package util

import scala.xml.XML

/**
 * Created by Filip on 2015-05-05.
 */
object XmlReader {
  /**
   * Otwiera plik XML i zwraca jako obiekt String.
   * @param path Path to xml file
   * @return String representation of xml file
   */
  def getXMLAsString(path:String):String = {
    XML.loadFile(path).toString()
  }

  /**
   * Get
   * @param path
   * @return
   */
  def getXML(path:String) = {
    XML.loadFile(path)
  }

}
