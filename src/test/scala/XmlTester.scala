import java.awt.Dimension

import model.Frame
import test.xml._
import util.{XmlParser, XmlReader}

import scala.swing.{Button, Label, FileChooser, MainFrame}

/**
 * Created by fhalemba on 2015-05-16.
 */


class A {
  def a{
    println("AAAAAAA")
  }
}

class XmlTester extends App{

    //Some fun with xml as String
    val xmlText = XmlReader.getXMLAsString("test.xml")
    val frame:Frame = XmlParser.getXMLAsFrame(xmlText)
    println(frame)
    println(xmlText)
    //println(frame.buttons.button.map(println))


    //Some fun with XPath
    val xml = XmlReader.getXML("test.xml")
    val title = (xml \\ "title").text
    val id = (xml \\ "button" \\ "id").text
    println("id " + id)
    println("title " + title)



    val classLoader = getClass.getClassLoader;
    val clas = classLoader.loadClass("test.A")
    println(clas)
    clas.getDeclaredMethod("a").invoke(new A,"",new AnyRef)


    //val id = (xml \\ "button" \\ "@id").text zwrocilo by  <button id="1">


    //Here fun with GUI
    //val ui = new UI(frame)
    //ui.visible = true

}

class UI(f:Frame) extends MainFrame {
    new FileChooser()
    title = f.title
    preferredSize = new Dimension(f.height, f.weight)
    contents = new Label("Here is the contents!")
    contents = Button("Press me, please") { println("Thank you") }
    centerOnScreen
}